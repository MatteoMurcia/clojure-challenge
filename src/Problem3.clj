(ns Problem3
  (:require [clojure.test :refer [deftest is testing]]
            [invoice-item :as ii]))

(deftest test-subtotal-with-zero-discount-rate
  (testing "The subtotal need to be the same as the precise-price* precise-quantity for discount-rate 0"
    (let [item {:invoice-item/precise-price 100.0
                :invoice-item/precise-quantity 2.0}]
      (is (= 200.0 (ii/subtotal item))))))


(deftest test-subtotal-with-discount-rate
  (testing "subtotal must be equal to precise-price * precise-quantity * (1 - discount-rate / 100) for a discount rate > 0"
    (let [item {:invoice-item/precise-price 100.0
                :invoice-item/precise-quantity 2.0
                :invoice-item/discount-rate 10}]
      (is (= 180.0 (ii/subtotal item))))))


(deftest test-subtotal-with-quantity-0
  (testing "the subtotal must be equal to 0 for the precise-amount 0"
    (let [item {:invoice-item/precise-price 100.0
                :invoice-item/precise-quantity 0.0
                :invoice-item/discount-rate 10}]
      (is (= 0.0 (ii/subtotal item))))))


(deftest test-subtotal-with-price-0
  (testing "the subtotal should be equal to 0 for the precise-price 0"
    (let [item {:invoice-item/precise-price 0.0
                :invoice-item/precise-quantity 2.0
                :invoice-item/discount-rate 10}]
      (is (= 0.0 (ii/subtotal item))))))

(deftest test-subtotal-with-price-and-quantity-0
  (testing "The subtotal must be equal to 0 for precise-price and precise-quantity 0"
    (let [item {:invoice-item/precise-price 0.0
                :invoice-item/precise-quantity 0.0
                :invoice-item/discount-rate 10}]
      (is (= 0.0 (ii/subtotal item))))))