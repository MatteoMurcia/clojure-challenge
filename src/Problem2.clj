(ns Problem2
  (:require
    [clojure.spec.alpha :as s]
    [clojure.data.json :as json]
    [invoice-spec :as spec]))

(import 'java.text.SimpleDateFormat)
(import 'java.util.Date)

; invoice example that passes spec
(def invoice-data
  {:invoice/issue-date (java.util.Date.)
   :invoice/customer {:customer/name "ANDRADE RODRIGUEZ MANUEL ALEJANDRO"
                      :customer/email "cgallegoaecu@gmail.com"}
   :invoice/items [{:invoice-item/price 10000.00
                    :invoice-item/quantity 1.00
                    :invoice-item/sku "SUPER-1"
                    :invoice-item/taxes [{:tax/category :iva
                                          :tax/rate 5}]}
                   {:invoice-item/price 20000.00
                    :invoice-item/quantity 1.00
                    :invoice-item/sku "SUPER-2"
                    :invoice-item/taxes [{:tax/category :iva
                                          :tax/rate 19}]}
                   {:invoice-item/price 30000.00
                    :invoice-item/quantity 1.00
                    :invoice-item/sku "SUPER-3"
                    :invoice-item/taxes [{:tax/category :iva
                                          :tax/rate 19}]}]})


(def date-format "dd/MM/yyyy")

(defn string-to-date [date-string]
  (let [date-format (SimpleDateFormat. date-format)
        date (.parse date-format date-string)]
    date))

(defn generate-invoice [file-name]
  (let [json-data (slurp file-name)
        parsed-data (json/read-str json-data :key-fn keyword)]
    (let [invoice (get-in parsed-data[:invoice])
          issue-date (get-in invoice[:issue_date])
          customer (get-in invoice[:customer])
          items (get-in invoice[:items])
          invoice-map {:invoice/issue-date (string-to-date issue-date)
                       :invoice/customer {:customer/name (get-in customer [:company_name])
                                          :customer/email (get-in customer [:email])}
                       :invoice/items (into [] (map (fn [item]
                                                      {:invoice-item/price (get-in item [:price])
                                                       :invoice-item/quantity (get-in item [:quantity])
                                                       :invoice-item/sku (get-in item [:sku])
                                                       :invoice-item/taxes (into [] (map (fn [tax]
                                                                                           {:tax/category (if (= (get-in tax[:tax_category]) "IVA") :iva :iva)
                                                                                            :tax/rate (if (not(get-in tax[:tax_rate])) (double (get-in tax[:tax_rate])) 1.0)})
                                                                                         (get-in item [:taxes])))})
                                                    items))}]
      (if (s/valid? ::spec/invoice invoice-map)
        invoice-map
        (throw (ex-info "Invoice does not pass specification." invoice-map)))
      )))





(defn run [opts]
  (print (generate-invoice "invoice.json")))
; run using the command: clj -X Problem2/run
