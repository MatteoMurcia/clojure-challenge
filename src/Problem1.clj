(ns Problem1)


(def invoice (clojure.edn/read-string (slurp "invoice.edn")))

(defn invoice-items [invoice]
  (->> invoice
       :invoice/items
       (filter #(or (some (fn [param1] (= 19 (:tax/rate param1))) (:taxable/taxes %))
                    (some (fn [param1] (= 1 (:retention/rate param1))) (:retentionable/retentions %))))))


(defn run [opts]
  (print (invoice-items invoice)))
; run using the command: clj -X Problem1/run
